import musicplayer, sys, os, fnmatch, random, pprint
import tkinter as Tkinter

class Song:
    def __init__(self, fn):
        self.url = fn
        self.f = open(fn,'rb')
    # `__eq__` is used for the peek stream management
    def __eq__(self, other):
        return self.url == other.url
    # this is used by the player as the data interface
    def readPacket(self, bufSize,):
        return self.f.read(bufSize)
    def seekRaw(self, offset, whence):
        r = self.f.seek(offset, whence)
        return self.f.tell()

files = []
def getFiles(path):
    for f in sorted(os.listdir(path), key=lambda k: random.random()):
        f = os.path.join(path, f)
        if os.path.isdir(f): getFiles(f) # recurse
        if len(files) > 1000: break # break if we have enough
        if fnmatch.fnmatch(f, '*.*'): files.append(f)
getFiles(os.path.expanduser("~/Music"))
random.shuffle(files) # shuffle some more

i = 0

def songs():
    global i, files
    while True:
        yield Song(files[i])
        i += 1
        if i >= len(files): i = 0

def peekSongs(n):
    nexti = i + 1
    if nexti >= len(files): nexti = 0
    return map(Song, (files[nexti:] + files[:nexti])[:n])

# Create our Music Player.
player = musicplayer.createPlayer()
player.outSamplerate = 96000 # support high quality :)
player.queue = songs()
player.peekQueue = peekSongs

# Setup some shortcuts :)

# Setup a simple GUI.
window = Tkinter.Tk()
songLabel = Tkinter.StringVar()
songName = Tkinter.StringVar()
songTime = Tkinter.StringVar()



def onSongChange(**kwargs):
    songLabel.set(pprint.pformat(player.curSongMetadata))
    songName.set(os.path.basename(player.curSong.url))

def cmdPlayPause(*args): player.playing = not player.playing
def cmdNext(*args): player.nextSong()
def cmd_seek_back(*args): player.seekRel(-10)
def cmd_seek_fwd(*args): player.seekRel(10)

def refresh_time():
    songTime.set("Time: %.1f / %.1f" % (player.curSongPos or -1, player.curSongLen or -1))
    window.after(2, refresh_time)  # every 2ms

def refresh_title():
    title = ("Music Player", player.curSongMetadata)
    window.title(title)
    

Tkinter.Label(window, textvariable=songLabel).pack()
Tkinter.Label(window, textvariable=songTime).pack()
Tkinter.Button(window, text="Play/Pause", command=cmdPlayPause).pack()
Tkinter.Button(window, text="Next", command=cmdNext).pack()
Tkinter.Button(window, text="Seek -10", command=cmd_seek_back).pack()
Tkinter.Button(window, text="Seek +10", command=cmd_seek_fwd).pack()
refresh_time()
refresh_title()

player.onSongChange = onSongChange
player.playing = True # start playing
window.mainloop()

