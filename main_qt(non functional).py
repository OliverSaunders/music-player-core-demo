import musicplayer, sys, os, fnmatch, random, pprint
import tkinter as Tkinter

from PyQt5.QtWidgets import QApplication, QWidget
from PyQt5.QtGui import QIcon

class Song:
    def __init__(self, fn):
        self.url = fn
        self.f = open(fn,'rb')
    # `__eq__` is used for the peek stream management
    def __eq__(self, other):
        return self.url == other.url
    # this is used by the player as the data interface
    def readPacket(self, bufSize,):
        return self.f.read(bufSize)
    def seekRaw(self, offset, whence):
        r = self.f.seek(offset, whence)
        return self.f.tell()

files = []
def getFiles(path):
    for f in sorted(os.listdir(path), key=lambda k: random.random()):
        f = os.path.join(path, f)
        if os.path.isdir(f): getFiles(f) # recurse
        if len(files) > 1000: break # break if we have enough
        if fnmatch.fnmatch(f, '*.*'): files.append(f)
getFiles(os.path.expanduser("~/Music"))
random.shuffle(files) # shuffle some more

i = 0

def songs():
    global i, files
    while True:
        yield Song(files[i])
        i += 1
        if i >= len(files): i = 0

def peekSongs(n):
    nexti = i + 1
    if nexti >= len(files): nexti = 0
    return map(Song, (files[nexti:] + files[:nexti])[:n])

# Create our Music Player.
player = musicplayer.createPlayer()
player.outSamplerate = 96000 # support high quality :)
player.queue = songs()
player.peekQueue = peekSongs

def onSongChange(**kwargs):
	songLabel = songLabel.set(pprint.pformat(player.curSongMetadata))
	songName = songName.set(os.path.basename(player.curSong.url))
    
def cmdPlayPause(*args): player.playing = not player.playing
def cmdNext(*args): player.nextSong()

# Create our app
main_title = ("Music player - ",songName)

class App(QWidget):

    def __init__(self):
        super().__init__()
        self.title = main_title
        self.left = 10
        self.top = 10
        self.width = 640
        self.height = 480
        self.initUI()
        
    def initUI(self):
        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)
        self.show()
    
if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = App()
    sys.exit(app.exec_())

